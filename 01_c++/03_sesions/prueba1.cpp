/**
 * @file Template.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>
#include<math.h>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	int variableCualquiera=1;	//input  
	float variableFlotante=15.0;
	double variableDoblementeflotante=15.0;
	
	cout<<"\r\nEntra un numero cualquiera";
	cin>>variableCualquiera;
	cout<<"\r\nEl numero elevado al cubo es:";
	cout<<pow(variableCualquiera,3);	//output imprimir el resultado elevado al cubo
	return 0;
	}
	



