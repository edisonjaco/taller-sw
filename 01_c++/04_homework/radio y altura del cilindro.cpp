/**
 * @file tarea1.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief File description
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>
#include<math.h>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

//variables

int main(){
	float pi=3.1416;
	float altura=0.0;
	float radio=0.0;
	
	//data
	
	cout<<"\r\nEscriba la altura del cilindro";	//output variable to enter (altura)
	cin>>altura;	//input variable (altura)
	cout<<"\r\nEscriba el radio del cilindro";	//output variable to enter (radio)
	cin>>radio;	//input variable (radio)
	
	//operations
	
	cout<<"\r\nEl area lateral es:";
	cout<<2*pi*radio*altura;	//output result(area latera)
	cout<<"\r\nEl volumen es:";
	cout<<pow(radio,2)*pi*altura;	//output result(volumen)
	return 0;
	}
	



