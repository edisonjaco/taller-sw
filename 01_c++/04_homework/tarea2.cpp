/**
 * @file tarea3.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief File description
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>




using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

//variables

int main(){
	int desaprobados=0;	
	int aprobados=0;	
	int notables=0;	
	int sobresalientes=0;
	
	//data
	
	cout<<"\r\nEscriba la cantidad de alumnos aprobados";	//output variable to enter(aprobados)
	cin>>aprobados;	//input variable (aprovados)
	cout<<"\r\nEscriba la cantidad de alumnos desaprobados";	//output variable to enter(desaprobados)
	cin>>desaprobados;	//input variable (desaprobados)
	cout<<"\r\nEscriba la cantidad de alumnos notables:";	//output variable to enter(notables)
	cin>>notables;	//input variable (notables)
	cout<<"\r\nEscriba la cantidad de alumnos sobresalientes:";	//output variable to enter(sobresalientes)
	cin>>sobresalientes;	//input variable (sobresalientes)
	
	//operations
	
	cout<<"\r\nEl porcentaje de alumnos que aprobaron la asignatura es:";
	cout<<((aprobados+notables+sobresalientes)*100)/(aprobados+desaprobados+notables+sobresalientes);	//output result(aprobaron la asignatura)
	cout<<"%";	//output sign
	cout<<"\r\nEl porcentaje de alumnos aprobados es:";
	cout<<(aprobados*100)/(aprobados+desaprobados+notables+sobresalientes);	//output result (aprobados)
	cout<<"%";	//output sign
	cout<<"\r\nEl porcentaje de alumnos desaprobados es:";
	cout<<(desaprobados*100)/(aprobados+desaprobados+notables+sobresalientes);	//output result (desaprobados)
	cout<<"%";	//output sign
	cout<<"\r\nEl porcentaje de alumnos notables es:";
	cout<<(notables*100)/(aprobados+desaprobados+notables+sobresalientes);	//output result (notables)
	cout<<"%";	//output sign
	cout<<"\r\nEl porcentaje de alumnos sobresalientes es:";
	cout<<(sobresalientes*100)/(aprobados+desaprobados+notables+sobresalientes);	//output result (sobresalientes)
	cout<<"%";	//output sign
	return 0;
	}
	

