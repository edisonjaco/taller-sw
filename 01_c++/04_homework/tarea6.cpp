/**
 * @file tarea6.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief Desglosar cierta cantidad de segundos a su equivalente en d�as,
	horas, minutos y segundos.
 * @version 1.0
 * @date 26.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

//declaration of variables

int main(){
	
	float segundos=0.0;
	float dias=0.0;
	float horas=0.0;
	float minutos=0.0;	 	
	
	////Ask data throught the terminal
	
	cout<<"\r\nEscriba la cantidad de segundos\n";	
	cin>>segundos;	//input segundos
	
	//operations
	
	dias=segundos/86400;
	horas=segundos/3600;
	minutos=segundos/60;
	
	//results
	
	cout<<"\r\nEl equivalente en dias es: "<<dias;
	cout<<"\r\nEl equivalente en horas es: "<<horas;
	cout<<"\r\nEl equivalente en minutos es: "<<minutos;


	return 0;
	}
	


