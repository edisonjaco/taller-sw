/**
 * @file tarea6.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, mediante la siguiente f�rmula:
	F = G*masa1*masa2 / distancia2
	Donde G es la constante de gravitaci�n universal: G = 6.673 * 10-8 cm3/g.seg2

 * @version 1.0
 * @date 26.01.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/


#include<math.h>
#include<iostream>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

//declaration of variables

int main(){
	
	float gravedad=6.673;
	float masa1=0.0;
	float masa2=0.0;
	float distancia=0.0;	
	float Fatraccion=0.0; 	
	
	////Ask data throught the terminal
	
	cout<<"\r\nEscriba la masa del primero objeto\n";	
	cin>>masa1;											//input segundos
	cout<<"\r\nEscriba la masa del segundo objeto\n";
	cin>>masa2;											
	cout<<"\r\nEscriba la distancia\n";
	cin>>distancia;
	
	//operation
	
	Fatraccion=(gravedad*masa1*masa2)/pow(distancia,2);
	
	//results
	
	cout<<"\r\nLa fuerza de atraccion entre los dos objetos es: "<<Fatraccion;


	return 0;
	}
	
