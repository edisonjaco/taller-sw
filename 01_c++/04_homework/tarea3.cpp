/**
 * @file tarea2.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief File description
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

//variables

int main(){
	int hombres=0;	 
	int mujeres=0;	
	
	//data
	
	cout<<"\r\nEscriba cuantos hombres hay";	//output variable to enter(hombres)
	cin>>hombres;	//input variable (hombres)
	cout<<"\r\nEscriba cuantas mujeres hay";	//output variable to enter(mujeres)
	cin>>mujeres;	//input variable (mujeres)
	
	//operations
	
	cout<<"\r\nEl porcentaje de hombres es:";
	cout<<(hombres*100)/(hombres+mujeres);	//output result (hombres)
	cout<<"%";	//output sign 
	cout<<"\r\nEl porcentaje de mujeres es:";
	cout<<(mujeres*100)/(hombres+mujeres);	//output result (mujeres)
	cout<<"%";	//output sign
	return 0;
	}
	


