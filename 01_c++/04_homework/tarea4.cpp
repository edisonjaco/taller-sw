/**
 * @file Tarea4.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief File description
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/


//Variables
int main(){
	float temperatura=0.0;	
	float agua=0.0;	
	
	//data
	
	cout<<"\r\nEscriba la temperatura en grados celcius";	//output variable to enter (temperatura)
	cin>>temperatura;	//input variable (temperatura)
	cout<<"\r\nEscriba la cantidad de agua";	//output variable to enter (agua)
	cin>>agua;	//input variable (agua)
	
	//operations
	
	cout<<"\r\nLa temperatura en grados Farenheit es:";
	cout<<((temperatura*9)/5)+32;	//output result(farenheit)
	cout<<"F";	//output sign
	cout<<"\r\nLa cantidad de agua en milimetros es:";
	cout<<agua*25.5;	//output result(agua)
	cout<<"mm";	//output sign
	return 0;
	}
	
