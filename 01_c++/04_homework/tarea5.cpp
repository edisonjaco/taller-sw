/**
 * @file tarea5.cpp
 * @author Edison Jaco (edisonj257@hotmail.com)
 * @brief File description
 * @version 1.0
 * @date 04.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/



#include<iostream>



using namespace std;
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

//variables

int main(){
	float automovil=0.0;	 	
	
	//data
	
	cout<<"\r\nEscriba el precio del automovil:S/.";	//output variable to enter(automovil)
	cin>>automovil;	//input variable (automovil)
	
	//operations
	
	cout<<"\r\nEl costo del automovil para el consumidor es:";
	cout<<"S/.";	//output sign
	cout<<(((automovil*12)/(100))+((automovil*6)/(100))+(automovil));	//output result (costo del automovil)
	return 0;
	}
	


